# Styleguide

### Тэги: #styleguide

Частью Styleguide является настроенный Eslint.
Рекомендуется настроить hotkey для форматирования кода по линтеру.
## БЭМ
### Классы и модификаторы
1) <span style="color: aqua;">my-awesome-class</span> - название класса через kebab-case
```html
// GOOD
<div class="my-class" />
// WRONG
<div class="myClass" />
```
2) <span style="color: aqua;">my-awesome-class_child</span> - название для ребенка через две земли.
```html
// GOOD
<div class="my-class">
	<span class="my-class__child"/>
</div>
// WRONG
<div class="my-class">
	<span class="my-class-child"/>
</div>
```
3) <span style="color: red;"> my-awesome-class__child__child </span> - нельзя допускать двойную вложенность детей. Если такая необходимость возникает, тогда нужно делать отдельный компонент.
```html
// GOOD
// VButton.vue
<button href="/" class="v-button">
	<span class="v-button__text"> // здесь можно было бы использовать слоты, но для примера испльзования классов будет так.
		1
	</span>
</button>

// anotherComponent.vue
<div class="my-class">
	<v-button />
</div>
// WRONG
<div class="my-class">
	<button href="/" class="my-class__button">
		<span class="my-class_button__text">
			1
		</span>
	</button>
</div>
```
4) <span style="color: aqua;">my-awesome-class__child-long-name</span> - если название класса для ребенка сложно составное, то используй kebab-case.
<span style="color: red;">ВЕЗДЕ ИСПОЛЬЗУЙ kebab-case</span>
```html
// GOOD
<div class="my-class">
	<span class="my-class__child-long-name" />
</div>
// WRONG
<div class="my-class">
	<span class="my-class__childLongName" />
</div>
```
5) <span style="color: lime;"> \_active, \_hover </span>- модификаторы состояний через землю.
```html
// GOOD
<div class="my-class">
	<span class="my-class__child_active" />
</div>
// WRONG
<div class="my-class">
	<span class="my-class_child active" />
</div>
```
6) <span style="color: lime;"> black </span> - модификаторы цвета, размера и тп - тоже через землю.
```html
// GOOD
<div class="my-class">
	<span class="my-class__child_black" />
</div>
// WRONG
<div class="my-class">
	<span class="my-class_child _black" />
</div>
```

### Nested class
```scss
.list {
    display: block;
    &__item {
        background: red;
    }
}
```

Использовать Nested разрешено, но нельзя превышать 2 вложенности: (не считая состояний и псевдоклассов)

```scss
// GOOD
/* обычная вложенность */
.list {
    display: block;
    &__item {
		background: red;
		& .text {
	   		color: red;
		}
    }
}

/* вложенность псевдо[элементов/классов] */
.list2 {
    display: block;
    &__item2 {
		background: red;
		& .text2 {
	   		color: red;
			&:before {
				/*...*/
				&:hover {
					/*...*/
				}
			}
		}
		& .text3 {
			/*...*/
		}
		& .link3 {
			/*...*/
			&:before {
				/*...*/
				&:hover {
					/*...*/
				}
			}
		}
    }
}

// WRONG
/* обычная вложенность */
.list {
    display: block;
    &__item {
			background: red;
		& .text {
	   		color: red;
			&__link {
				/*...*/
				& .text1 {
					/*...*/
				}
			}
		}
    }
}

/* вложенность псевдо[элементов/классов] */
.list2 {
    display: block;
    &__item2 {
		background: red;
		& .text2 {
	   		color: red;
			& .text3 {
				/*...*/
				& .link3 {
					/*...*/
					&:before {
						/*...*/
						&:hover {
							/*...*/
						}
					}
				}
			}
		}
    }
}
```

Неправильный пример выше всё ещё возможно читать, но, как известно, обычно у корневого класса не один ребенок и тогда начинаются проблемы. Для этого и введено правило.
## Названия для методов
- <span style="color: yellow;">getSome</span> - Возвращает значение. Аргумент опционален.
- <span style="color: yellow;">isSome</span> - Возвращает значение. Не принимает аргумент.
- <span style="color: yellow;">fetchSome</span> - Запрос значения (с сервера) и запись его в глобальную переменную компонента.
- <span style="color: yellow;">setSome</span> - Присваивание значения. Аргумент опционален.
- <span style="color: yellow;">toggleSome</span> - Переключение стейта между 2-мя вариантами, инкапсулированными в функцию. Не принимает аргумент.
- <span style="color: yellow;">checkSome</span> - Проверка значения по каким-то критериям. Возвращает значение, аргумент опционален.
- <span style="color: yellow;">resetSome</span> - Сбросить значение. Не принимает аргумент

## Названия для computed
Не использовать префиксы из названий методов.

```js
// GOOD
newsList() {
	return []
}
// WRONG
getList() {
	return []
}
```
## Общие правила
### Комментарии к calc значениям
```css
/* Ширина страницы - ширина блока */
width: calc(100% - 25px);
```
### v-for
Именуй v-for по принципу, если перебираемый объект имеет единственное число, то итерируй единственное число (tasks => task), если не имеет, то добавляй слово item.

Если из итерируемого сложносоставного объекта можно выделить общий термин единственного числа, то используй его. (например, newsCards => card), в зависимости от окружений. Т.е если рядом есть соседи v-for=”... of …cards”, то используй newsCards).

#### :key

Для :key не используем index, из-за проблем с шафлом массива.

```html
// GOOD
<div v-for="image of images" :key="image.id" />
<div v-for="newsItem of news" :key="newsItem.name" />
<div v-for="slide of sliderData" :key="slide.id" />

// WRONG
<div v-for="(item, index) of images" :key="index" />
<div v-for="item of images" :key="item" />
<div v-for="item of news" :key="item" />
<div v-for="item of improvementsSliderData" :key="item" />
```

### Работа с git и коммиты

Название ветки задаём по принципу [[ номер задачи ]] пример ep-640

Для описания коммита используем варианты действий из данного списка: 
- feature — используется при добавлении новой функциональности уровня приложения
- fix — если исправили какую-то серьезную багу
- docs — всё, что касается документации
- style — исправляем опечатки, исправляем форматирование
- refactor — рефакторинг кода приложения
- test — всё, что связано с тестированием
- chore — обычное обслуживание кода

после указываем в скобках название файла и путь с которым работали

feature(@/components/someComponent.vue)

и в конце пишем что сделали на русском языке(для удобства чтения).

- feature(@/components/someComponent.vue) фукнционал для получения данных в таблицу
- fix(@/components/someComponent.vue) отображение стоимости в таблице
